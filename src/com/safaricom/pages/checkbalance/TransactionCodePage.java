package com.safaricom.pages.checkbalance;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class TransactionCodePage {
 public AndroidDriver<MobileElement> driver;
  
  public TransactionCodePage() {
	  
  }
  public TransactionCodePage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/labelTitle")
  public AndroidElement transactioncode_labeltitle;
   
  @AndroidFindBy(id="com.selfcare.safaricom:id/inputTransactionCode")
  public AndroidElement inputTransactionCode;
 
  @AndroidFindBy(id="com.selfcare.safaricom:id/buttonNegative")
  public AndroidElement cancel_btn;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/buttonPositive")
  public AndroidElement submit_btn  ;
  
  }
