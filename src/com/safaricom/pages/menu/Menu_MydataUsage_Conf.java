package com.safaricom.pages.menu;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class Menu_MydataUsage_Conf {
	public AndroidDriver<MobileElement> driver;
  
  public Menu_MydataUsage_Conf() {
	  
  }
  public Menu_MydataUsage_Conf(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/dlg_title")
  public AndroidElement dlg_title; 
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_confirm_message")
  public AndroidElement tv_confirm_message;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_cancel")
  public AndroidElement btn_cancel;
    
  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_continue")
  public AndroidElement btn_continue;
    
  }
