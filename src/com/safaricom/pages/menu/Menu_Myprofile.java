package com.safaricom.pages.menu;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class Menu_Myprofile {
	public AndroidDriver<MobileElement> driver;
  
  public Menu_Myprofile() {
	  
  }
  public Menu_Myprofile(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
  public AndroidElement myProfile_title;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/img_profile")
  public AndroidElement img_profile;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_name")
  public AndroidElement tv_name;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/et_edit_email")
  public AndroidElement et_edit_email;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_imi_number")
  public AndroidElement tv_imi_number;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_puk_number")
  public AndroidElement tv_puk_number;
  
}
