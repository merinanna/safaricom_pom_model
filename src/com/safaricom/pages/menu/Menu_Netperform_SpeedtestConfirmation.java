package com.safaricom.pages.menu;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class Menu_Netperform_SpeedtestConfirmation {
	public AndroidDriver<MobileElement> driver;
  
  public Menu_Netperform_SpeedtestConfirmation() {
	  
  }
  public Menu_Netperform_SpeedtestConfirmation(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_message")
  public AndroidElement tv_message;
 
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_cancel")
  public AndroidElement cancel_btn;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_ok")
  public AndroidElement ok_btn;
  
}
