package com.safaricom.pages.menu;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class Menu_knowledgebase {
	public AndroidDriver<MobileElement> driver;
  
  public Menu_knowledgebase() {
	  
  }
  public Menu_knowledgebase(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
  public AndroidElement knowledgebase_title; 

  @AndroidFindBy(xpath="//android.widget.TextView[@text='Web page not available']")
  public AndroidElement errorpage_subtitle;
    
  }
