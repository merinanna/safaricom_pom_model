package com.safaricom.pages.menu;

import java.util.List;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class Menudrawer {
	public AndroidDriver<MobileElement> driver;
  
  public Menudrawer() {
	  
  }
  public Menudrawer(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @FindBy(how = How.ID,using="com.selfcare.safaricom:id/tv_title")
	public  List<MobileElement> L1;
  
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Home']")
  public AndroidElement home_click;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Settings']")
  public AndroidElement settings_clicks;
   
  @AndroidFindBy(xpath="//android.widget.TextView[@text='My Profile']")
  public AndroidElement myprofile_click;

  @AndroidFindBy(xpath="//android.widget.TextView[@text='M-PESA']")
  public AndroidElement mpesa_click;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Services']")
  public AndroidElement services_click;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='My Account']")
  public AndroidElement myaccount_click;
   
  @AndroidFindBy(xpath="//android.widget.TextView[@text='NetPerform']")
  public AndroidElement netperform_click ;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='My Data Usage']")
  public AndroidElement mydatausage_click;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Knowledge base']")
  public AndroidElement knowledgebase ;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Store Locator']")
  public AndroidElement storelocator_click;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Tell a friend']")
  public AndroidElement tellafriend;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Feedback & Rating']")
  public AndroidElement feedbacknrating_click;
   
  @AndroidFindBy(xpath="//android.widget.TextView[@text='About Us']")
  public AndroidElement aboutus_click ;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='About Us']")
  public AndroidElement contactus_click;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Back to Platinum']")
  public AndroidElement backtoplatinum_click ;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Log Out']")
  public AndroidElement logout_click;
    

    
}
