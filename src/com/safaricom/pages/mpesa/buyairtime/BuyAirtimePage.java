package com.safaricom.pages.mpesa.buyairtime;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import com.sun.xml.internal.bind.v2.schemagen.xmlschema.List;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AndroidFindByAllSet;
import io.appium.java_client.pagefactory.AndroidFindBys;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class BuyAirtimePage {
 
	public AndroidDriver<MobileElement> driver;
	public BuyAirtimePage()
	{
		
	}
	
	public BuyAirtimePage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
	public AndroidElement buyAirtime_title;
	
	
	//My Number
	
	@AndroidFindBy(xpath="//android.widget.RadioButton[@text='My Number']")
	public AndroidElement myNumberClick;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/et_pin")
	public AndroidElement myNumber_et_pin;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_topup")
	public AndroidElement myNumber_continue;
		
	//Other Number
		
	@AndroidFindBy(xpath="//android.widget.RadioButton[@text='Other Number']")
	public AndroidElement otherNumberClick;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/edt_mobilenumber")
	public AndroidElement otherNumber_edt_mobilenumber;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/et_pin")
	public AndroidElement otherNumber_et_pin;
		
		
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_topup")
	public AndroidElement otherNumber_continue;
	
	}
