package com.safaricom.pages.mpesa.lipanampesa;


import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class LipanaMPesaPage {
 
	public AndroidDriver<MobileElement> driver;
	public LipanaMPesaPage()
	{
		
	}
	
	public LipanaMPesaPage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
	public AndroidElement lipanampesa_title;
	
	//PayBill
		
	@AndroidFindBy(id="com.selfcare.safaricom:id/tvPaybill")
	public AndroidElement paybillClick;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/edt_buss_no")
	public AndroidElement paybill_edt_buss_no;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/edt_lipa_account_number")
	public AndroidElement paybill_edt_account_number;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/edt_bill_amount")
	public AndroidElement paybill_edt_bill_amount;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_bill_continue")
	public AndroidElement paybill_continue;
		
	//By Goods
		
	@AndroidFindBy(id="com.selfcare.safaricom:id/tvBuygoods")
	public AndroidElement BuygoodsClick;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/edt_till_no")
	public AndroidElement Buygoods_edt_till_no;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/edt_till_amount")
	public AndroidElement Buygoods_edt_till_amount;
		
		
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_buygoods_continue")
	public AndroidElement Buygoods_continue;
	
	}
