package com.safaricom.pages.mpesa.loansandsavings;


import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class Loansandsavingshomepage {
 
	public AndroidDriver<MobileElement> driver;
	public Loansandsavingshomepage()
	{
		
	}
	
	public Loansandsavingshomepage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	//Loans and Savings Home Page
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
	public AndroidElement loansandsavings_title;
	
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/card_mshwari")
	public AndroidElement mshwari_Click;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/card_kcb_mpesa")
	public AndroidElement kcb_mpesa_click;
	
	
	}
