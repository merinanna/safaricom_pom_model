package com.safaricom.pages.mpesa.loansandsavings;


import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class LockSavingsAccount_OpenLockAccountPage {
 
	public AndroidDriver<MobileElement> driver;
	public LockSavingsAccount_OpenLockAccountPage()
	{
		
	}
	
	public LockSavingsAccount_OpenLockAccountPage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	
	  
	  //Open Savings Account
	
	 @AndroidFindBy(id="com.selfcare.safaricom:id/title_lock_ac")
	  public AndroidElement openlockaccount_title;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/spin_acc_from")
	  public AndroidElement acc_from_click;
	  
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='M-PESA']")
	  public AndroidElement mpesa_click;
	  
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='M-Shwari']")
	  public AndroidElement mshwari_click;
	  
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/et_target_amount")
	  public AndroidElement et_target_amount;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/et_period")
	  public AndroidElement et_period;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/et_amount")
	  public AndroidElement et_amount;
	  
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_continue")
	  public AndroidElement btn_continue;
	 
	  
	
	
	}
