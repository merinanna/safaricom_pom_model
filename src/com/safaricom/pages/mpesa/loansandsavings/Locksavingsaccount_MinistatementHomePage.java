package com.safaricom.pages.mpesa.loansandsavings;


import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class Locksavingsaccount_MinistatementHomePage {
 
	public AndroidDriver<MobileElement> driver;
	public Locksavingsaccount_MinistatementHomePage()
	{
		
	}
	
	public Locksavingsaccount_MinistatementHomePage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	
	  
	  //Ministatement
	
		@AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
	  public AndroidElement ministatement_title;
	
		 @AndroidFindBy(id="com.selfcare.safaricom:id/tvStmtHeader")
		  public AndroidElement ministatement_tvStmtHeader;
		 
			@AndroidFindBy(className="android.widget.ImageButton")
			public AndroidElement backclick;	  
	}
