package com.safaricom.pages.mpesa.loansandsavings;


import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class Locksavingsaccount_OpenLockAccountConfirmationPage {
 
	public AndroidDriver<MobileElement> driver;
	public Locksavingsaccount_OpenLockAccountConfirmationPage()
	{
		
	}
	
	public Locksavingsaccount_OpenLockAccountConfirmationPage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	
	 //Savings 
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_dialog_title")
	  public AndroidElement openlockaccount_txt_dialog_title;         //Confirmation - Open Account
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_dialog_amount")
	  public AndroidElement txt_dialog_amount;
	  	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_mpesa_sendto_value")
	  public AndroidElement tv_mpesa_sendto_value;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_mpesa_mobile_value")
	  public AndroidElement tv_mpesa_mobile_value;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_mpesa_agent_value")
	  public AndroidElement tv_mpesa_agent_value;
	  	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_cancel_dilaog")
	  public AndroidElement txt_cancel_dilaog;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_continue_dilaog")
	  public AndroidElement txt_continue_dilaog;
	  
	
	
	}
