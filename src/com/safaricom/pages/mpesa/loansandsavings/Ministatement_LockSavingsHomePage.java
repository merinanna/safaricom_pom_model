package com.safaricom.pages.mpesa.loansandsavings;


import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class Ministatement_LockSavingsHomePage {
 
	public AndroidDriver<MobileElement> driver;
	public Ministatement_LockSavingsHomePage()
	{
		
	}
	
	public Ministatement_LockSavingsHomePage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	

	  // Lock Savings Account Ministatement Home Page

	  @AndroidFindBy(id="com.selfcare.safaricom:id/tvStmtHeader")
	  public AndroidElement locksaviingsministatement_header;  //Lock Savings Account Mini Statement
	
	  
	}
