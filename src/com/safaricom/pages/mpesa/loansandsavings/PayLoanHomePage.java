package com.safaricom.pages.mpesa.loansandsavings;


import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class PayLoanHomePage {
 
	public AndroidDriver<MobileElement> driver;
	public PayLoanHomePage()
	{
		
	}
	
	public PayLoanHomePage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	
	  
	  //Pay Loan
	
		 @AndroidFindBy(id="com.selfcare.safaricom:id/title_loan")
		  public AndroidElement payloan_title;
		  
		  @AndroidFindBy(id="com.selfcare.safaricom:id/spinnerLoanFrom")
		  public AndroidElement loan_from_click;
		  
		  @AndroidFindBy(xpath="//android.widget.TextView[@text='M-PESA']")
		  public AndroidElement mpesa_click;
		  
		  @AndroidFindBy(xpath="//android.widget.TextView[@text='M-Shwari']")
		  public AndroidElement mshwari_click;
		  		  
		  @AndroidFindBy(id="com.selfcare.safaricom:id/et_amount")
		  public AndroidElement loan_et_amount;
		  
		  
		  @AndroidFindBy(id="com.selfcare.safaricom:id/loan_btn_text")
		  public AndroidElement loan_btn_continue;
	  
	  
	
	
	}
