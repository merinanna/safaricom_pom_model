package com.safaricom.pages.mpesa.loansandsavings;


import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class RequestLoanHomePage {
 
	public AndroidDriver<MobileElement> driver;
	public RequestLoanHomePage()
	{
		
	}
	
	public RequestLoanHomePage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	
	  
	  //Savings 
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/title_loan")
	  public AndroidElement requestloan_title;				//Request Loan
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/et_amount")
	  public AndroidElement loan_et_amount;
	  
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/loan_btn_text")
	  public AndroidElement loan_btn_continue;
	  
	  
	
	
	}
