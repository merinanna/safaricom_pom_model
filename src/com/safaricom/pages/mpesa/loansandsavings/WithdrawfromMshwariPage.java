package com.safaricom.pages.mpesa.loansandsavings;


import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class WithdrawfromMshwariPage {
 
	public AndroidDriver<MobileElement> driver;
	public WithdrawfromMshwariPage()
	{
		
	}
	
	public WithdrawfromMshwariPage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	
	  
	  //Savings 
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/title_mshwari")
	  public AndroidElement withdrawfromshwari_title;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/et_mshwari_amount")
	  public AndroidElement wfm_et_mshwari_amount;
	  
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_continue")
	  public AndroidElement withdrawfrommshwari_btn_continue;
	  
	  
	
	
	}
