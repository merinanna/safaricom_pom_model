package com.safaricom.pages.mpesa.myaccount;


import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class MPesaMyaccountCostCalculatorHomePage {
 
	public AndroidDriver<MobileElement> driver;
	public MPesaMyaccountCostCalculatorHomePage()
	{
		
	}
	
	public MPesaMyaccountCostCalculatorHomePage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	//My Account Home Page
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
	public AndroidElement costcalculator_title; //COST CALCULATOR
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/tvsendWithdrawLabel")
	public AndroidElement tvsendWithdrawLabel;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/tvMpesaLabel")
	public AndroidElement tvMpesaLabel;
	
	
	
	}
