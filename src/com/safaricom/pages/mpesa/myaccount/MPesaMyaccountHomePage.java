package com.safaricom.pages.mpesa.myaccount;


import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class MPesaMyaccountHomePage {
 
	public AndroidDriver<MobileElement> driver;
	public MPesaMyaccountHomePage()
	{
		
	}
	
	public MPesaMyaccountHomePage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	//My Account Home Page
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
	public AndroidElement myaccount_txt_title;
	
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='Cost Calculator']")
	public AndroidElement costcalculator_Click;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='M-PESA 1 TAP']")
	public AndroidElement mpesa1tap_Click;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='M-PESA Unlock']")
	public AndroidElement mpesaunlock_Click;
	
	
	
	}
