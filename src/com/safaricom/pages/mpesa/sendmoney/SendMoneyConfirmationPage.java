package com.safaricom.pages.mpesa.sendmoney;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class SendMoneyConfirmationPage {
 
	public AndroidDriver<MobileElement>  driver;
	public SendMoneyConfirmationPage()
	{
		
	}
	
	public SendMoneyConfirmationPage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	//<-------------------------Send to One-------------------------------->
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_dialog_title")
	public AndroidElement confirmation_title;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_mpesa_mobile_value")
	public AndroidElement tv_mpesa_mobile_value;
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_mpesa_agent_value")
	public AndroidElement tv_mpesa_agent_value;
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_mpesa_amount_value")
	public AndroidElement tv_mpesa_amount_value;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_cancel_dilaog")
	public AndroidElement SendtoOnecancel_dilaog;
	
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_continue_dilaog")
	public AndroidElement SendtoOnecontinue_dilaog;
	
}
