package com.safaricom.pages.mpesa.sendmoney;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class SendMoneyPage {
 
	public AndroidDriver<MobileElement>  driver;
	public SendMoneyPage()
	{
		
	}
	
	public SendMoneyPage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
	public AndroidElement SendMoney_title;
	
	
	//<-------------------------Send to One-------------------------------->
	@AndroidFindBy(id="com.selfcare.safaricom:id/buttonSendToOneToggle")
	public AndroidElement SendtoOneClick;
	@AndroidFindBy(id="com.selfcare.safaricom:id/inputSendToOnePhoneNumber")
	public AndroidElement SendtoOnePhoneNumber;
	@AndroidFindBy(id="com.selfcare.safaricom:id/inputSendToOnePhoneAmount")
	public AndroidElement SendtoOnePhoneAmount;
	@AndroidFindBy(id="com.selfcare.safaricom:id/buttonSendToOneContinue")
	public AndroidElement SendtoOneContinue;
		
	
	//<-------------------------Send to Other-------------------------------->
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/buttonSendToOtherToggle")
	public AndroidElement SendtoOtherClick;
	@AndroidFindBy(id="com.selfcare.safaricom:id/inputSendToOtherPhoneNumber")
	public AndroidElement SendtoOtherPhoneNumber;
	@AndroidFindBy(id="com.selfcare.safaricom:id/inputSendToOtherAmount")
	public AndroidElement SendtoOtherPhoneAmount;
	@AndroidFindBy(id="com.selfcare.safaricom:id/buttonSendToOtherContinue")
	public AndroidElement SendToOtherContinue;
	@AndroidFindBy(id="com.selfcare.safaricom:id/button")
	public AndroidElement errormsg_close;	
	@AndroidFindBy(className="android.widget.ImageButton")
	public AndroidElement backclick;
	
	
	
}
