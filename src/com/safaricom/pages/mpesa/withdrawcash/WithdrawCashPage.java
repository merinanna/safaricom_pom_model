package com.safaricom.pages.mpesa.withdrawcash;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import java.util.List;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AndroidFindByAllSet;
import io.appium.java_client.pagefactory.AndroidFindBys;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class WithdrawCashPage {
 
	public AndroidDriver<MobileElement> driver;
	public WithdrawCashPage()
	{
		
	}
	
	public WithdrawCashPage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
	public AndroidElement withdrawcash_title;
	
	//Withdraw From Agent
	
//	java.util.List<MobileElement> L1 = driver.findElements(By.id("com.selfcare.safaricom:id/iv_expand_ico")); 
//	L1.get(1).click();
	
	
	
	@FindBy(how = How.ID,using="com.selfcare.safaricom:id/iv_expand_ico")
	public  List<MobileElement> L1;
	
//	@AndroidFindBy(id="com.selfcare.safaricom:id/iv_expand_ico")
//	public AndroidElement select;
	
	//public java.util.List<MobileElement> L1= (java.util.List<MobileElement>) select;
	


	
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/edt_mobilenumber")
	public AndroidElement edt_mobilenumber_WFAgent;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/et_amount")
	public AndroidElement et_amount_WFAgent;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/continue_sf")
	public AndroidElement WFAgentContinueClick;
	
		
	
	//Withdraw From ATM
	
	
	

	
	@AndroidFindBy(id="com.selfcare.safaricom:id/edt_mobilenumber")
	public AndroidElement edt_mobilenumber_WFAtm;
	
		
	@AndroidFindBy(id="com.selfcare.safaricom:id/continue_sf")
	public AndroidElement WFATMContinueClick;
	
	
	
	@AndroidFindBy(className="android.widget.ImageButton")
	public AndroidElement backclick;
	
	
	
}
