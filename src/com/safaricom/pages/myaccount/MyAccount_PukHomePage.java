package com.safaricom.pages.myaccount;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class MyAccount_PukHomePage {

  public AndroidDriver<MobileElement> driver;
  
  public MyAccount_PukHomePage() {
	  
  }
  public MyAccount_PukHomePage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
  public AndroidElement txt_title;
  
  // PUK
  @AndroidFindBy(id="com.selfcare.safaricom:id/labelRequestPUK")
  public AndroidElement labelRequestPUK;
    
  @AndroidFindBy(id="com.selfcare.safaricom:id/rb_my_num")
  public AndroidElement rb_my_num_puk;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/iv_puk_copy")
  public AndroidElement iv_puk_copy;
    
  @AndroidFindBy(id="com.selfcare.safaricom:id/rb_other_num")
  public AndroidElement rb_other_num_puk ;
     
  @AndroidFindBy(id="com.selfcare.safaricom:id/edt_mobilenumber")
  public AndroidElement edt_mobilenumber;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/et_pin")
  public AndroidElement et_pin ;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_topup")
  public AndroidElement tv_topup_btn;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/cv_contact_us")
  public AndroidElement cv_contact_us;
}
