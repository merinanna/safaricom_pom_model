package com.safaricom.pages.services;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class FlexBuyOtherConfirmationPage {
	public AndroidDriver<MobileElement> driver;
  
  public FlexBuyOtherConfirmationPage() {
	  
  }
  public FlexBuyOtherConfirmationPage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  @AndroidFindBy(id="com.selfcare.safaricom:id/dlg_title")
  public AndroidElement confirmation_title;

  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_confirm")
  public AndroidElement confirm_msg;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tvOffer")
  public AndroidElement tvOffer;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tvValidity")
  public AndroidElement tvValidity;

  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_cancel")
  public AndroidElement btn_cancel;

  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_continue")
  public AndroidElement btn_continue;

  
}
