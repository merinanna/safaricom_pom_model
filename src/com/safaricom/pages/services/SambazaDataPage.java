package com.safaricom.pages.services;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class SambazaDataPage {
	public AndroidDriver<MobileElement> driver;
  
  public SambazaDataPage() {
	  
  }
  public SambazaDataPage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
  public AndroidElement data_details_title;
    
  @AndroidFindBy(id="com.selfcare.safaricom:id/edt_mobilenumber")
  public AndroidElement data_edt_mobilenumber;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/et_pin")
  public AndroidElement data_et_pin ;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_cancel")
  public AndroidElement data_btn_cancel ;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_ok")
  public AndroidElement data_btn_ok ;
  
}
