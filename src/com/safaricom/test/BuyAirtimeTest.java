package com.safaricom.test;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.mpesa.buyairtime.BuyAirtimeConfirmationPage;
import com.safaricom.pages.mpesa.buyairtime.BuyAirtimeFinalConfirmationPage;
import com.safaricom.pages.mpesa.buyairtime.BuyAirtimePage;
import com.safaricom.pages.sfc.MpesaHomePage;
import com.safaricom.pages.sfc.sfcHomePage;
import java.io.IOException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

@Test
public class BuyAirtimeTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public sfcHomePage sfcHomePageObject = new sfcHomePage(driver);
	public MpesaHomePage mpesaHomePageObject = new MpesaHomePage(driver);
	public BuyAirtimePage buyAirtimePageObject = new BuyAirtimePage(driver);
	public BuyAirtimeConfirmationPage buyAirtimeConfirmationPageObject = new BuyAirtimeConfirmationPage(driver);
	public BuyAirtimeFinalConfirmationPage buyAirtimeFinalConfirmationPageObject = new BuyAirtimeFinalConfirmationPage(
			driver);

	/*
	 * Verfy whether the elements "MY NUMBER,OTHER NUMBER" is present in Buy Airtime
	 * Home Page and Title should be "BUY AIRTIME"
	 */
	@Test
	public void BUYAIRTIME_TC_001() throws InterruptedException, IOException {
		Thread.sleep(5000);

		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
		}
		wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.buyairtimeClick)).click();
		String expectedtitle = "BUY AIRTIME1";
		String buyAirtime_title = buyAirtimePageObject.buyAirtime_title.getText();
		System.out.println(buyAirtime_title);
		Assert.assertEquals(buyAirtime_title, expectedtitle);
		wait.until(ExpectedConditions.elementToBeClickable(buyAirtimePageObject.myNumberClick)).click();
		Assert.assertEquals(true, buyAirtimePageObject.myNumber_et_pin.isDisplayed(),"Enter Pin is displayed");
		Assert.assertEquals(true, buyAirtimePageObject.myNumber_continue.isDisplayed(),"Continue button is displayed");
		wait.until(ExpectedConditions.elementToBeClickable(buyAirtimePageObject.otherNumberClick)).click();
		Assert.assertEquals(true, buyAirtimePageObject.otherNumber_edt_mobilenumber.isDisplayed(),"Mobile number field is displayed");
		Assert.assertEquals(true, buyAirtimePageObject.otherNumber_et_pin.isDisplayed(),"Enter pin for Other number is displayed");
		Assert.assertEquals(true, buyAirtimePageObject.otherNumber_continue.isDisplayed(),"Continue button is displayed");

	}

	/*
	 * Verify whether the "Buy Airtime : My Number" transaction is getting completed
	 * using valid Amount
	 */

	@Test
	public void BUYAIRTIME_TC_002() throws InterruptedException, IOException {

		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
		}

		if (!(buyAirtimePageObject.myNumberClick.isSelected())) {
			wait.until(ExpectedConditions.elementToBeClickable(buyAirtimePageObject.myNumberClick)).click();

		}

		wait.until(ExpectedConditions.elementToBeClickable(buyAirtimePageObject.myNumber_et_pin)).sendKeys("1234");
		wait.until(ExpectedConditions.elementToBeClickable(buyAirtimePageObject.myNumber_continue)).click();
		Thread.sleep(10000);
		String expected_confirmation_title = "Confirmation";
		String actual_confirmation_title = buyAirtimeConfirmationPageObject.confirmation_title.getText();
		Assert.assertEquals(actual_confirmation_title, expected_confirmation_title);
		String mpesa_agent_value = buyAirtimeConfirmationPageObject.txt_dialog_amount.getText();
		Assert.assertEquals(false, mpesa_agent_value.isEmpty(),"Agent Value is empty");
		String mpesa_amount_value = buyAirtimeConfirmationPageObject.tv_mpesa_sendto_value.getText();
		Assert.assertEquals(false, mpesa_amount_value.isEmpty(),"Amount Value is empty");
		Assert.assertEquals(true, buyAirtimeConfirmationPageObject.txt_continue_dilaog.isEnabled(),"Continue button is disabled");
		Assert.assertEquals(true, buyAirtimeConfirmationPageObject.txt_cancel_dilaog.isEnabled(),"Cancel button is disabled");
		wait.until(ExpectedConditions.elementToBeClickable(buyAirtimeConfirmationPageObject.txt_continue_dilaog))
				.click();
		Thread.sleep(1000);
		String expected_label = "Please wait to enter M-PESA PIN.";
		String actual_label = buyAirtimeFinalConfirmationPageObject.final_confirmation_label.getText();
		System.out.println(actual_label);
		wait.until(ExpectedConditions.elementToBeClickable(buyAirtimeFinalConfirmationPageObject.finalbutton_click))
				.click();
		Assert.assertEquals(actual_label, expected_label);

	}

	/*
	 * Verify whether the "Buy Airtime : Other Number" transaction is getting
	 * completed using valid phone number and valid amount
	 */

	@Test
	public void BUYAIRTIME_TC_003() throws InterruptedException, IOException {

		System.out.println("Landed Page : " + sfcHomePageObject.title.getText());

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.mpesa_click)).click();
		}
		wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.buyairtimeClick)).click();
		wait.until(ExpectedConditions.elementToBeClickable(buyAirtimePageObject.otherNumberClick)).click();
		wait.until(ExpectedConditions.elementToBeClickable(buyAirtimePageObject.otherNumber_edt_mobilenumber))
				.sendKeys("790771777");
		wait.until(ExpectedConditions.elementToBeClickable(buyAirtimePageObject.otherNumber_et_pin)).sendKeys("1234");
		wait.until(ExpectedConditions.elementToBeClickable(buyAirtimePageObject.otherNumber_continue)).click();

		Thread.sleep(10000);
		String expected_confirmation_title = "Confirmation";
		String actual_confirmation_title = buyAirtimeConfirmationPageObject.confirmation_title.getText();
		Assert.assertEquals(actual_confirmation_title, expected_confirmation_title);
		String mpesa_mobile_value = buyAirtimeConfirmationPageObject.txt_dialog_account_number.getText();
		Assert.assertEquals(false, mpesa_mobile_value.isEmpty(),"Mobile value is empty");
		String mpesa_agent_value = buyAirtimeConfirmationPageObject.txt_dialog_amount.getText();
		Assert.assertEquals(false, mpesa_agent_value.isEmpty(),"Agent value is empty");
		String mpesa_amount_value = buyAirtimeConfirmationPageObject.tv_mpesa_sendto_value.getText();
		Assert.assertEquals(false, mpesa_amount_value.isEmpty(),"Amount is empty");
		Assert.assertEquals(true, buyAirtimeConfirmationPageObject.txt_continue_dilaog.isEnabled(),"Continue  button is disabled");
		Assert.assertEquals(true, buyAirtimeConfirmationPageObject.txt_cancel_dilaog.isEnabled());
		wait.until(ExpectedConditions.elementToBeClickable(buyAirtimeConfirmationPageObject.txt_continue_dilaog))
				.click();
		Thread.sleep(1000);
		String expected_label = "Please wait to enter M-PESA PIN.";
		String actual_label = buyAirtimeFinalConfirmationPageObject.final_confirmation_label.getText();
		System.out.println(actual_label);
		wait.until(ExpectedConditions.elementToBeClickable(buyAirtimeFinalConfirmationPageObject.finalbutton_click))
				.click();
		Assert.assertEquals(actual_label, expected_label);

	}

}
