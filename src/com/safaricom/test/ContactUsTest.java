package com.safaricom.test;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.contactus.ContactUS_NewRequest;
import com.safaricom.pages.contactus.ContactUS_NewRequestFinalConfirmation;
import com.safaricom.pages.login.LocationPermissionPage;
import com.safaricom.pages.login.LoginFailurePage;
import com.safaricom.pages.login.LoginGeneratePinPage;
import com.safaricom.pages.login.LoginSuccessPage;
import com.safaricom.pages.login.ManagePhonePermissionPage;
import com.safaricom.pages.services.AccumulationHistoryConfirmationPage;
import com.safaricom.pages.services.BongaServicesPage;
import com.safaricom.pages.services.DSPlanActivateConfirmationPage;
import com.safaricom.pages.services.DSPlanActivateFinalConfirmationPage;
import com.safaricom.pages.services.DataandsmsplanservicesPage;
import com.safaricom.pages.services.DateSelectPage;
import com.safaricom.pages.services.FlexBuyBundlePage;
import com.safaricom.pages.services.FlexBuyBundle_ConfirmationPage;
import com.safaricom.pages.services.FlexBuyBundle_FinalConfirmationPage;
import com.safaricom.pages.services.FlexBuyBundle_PayWith;
import com.safaricom.pages.services.FlexBuyOtherConfirmationPage;
import com.safaricom.pages.services.FlexBuyOtherPage;
import com.safaricom.pages.services.FlexBuyother_FinalConfirmationPage;
import com.safaricom.pages.services.Flexservicespage;
import com.safaricom.pages.services.RedeemBongaPointConfirmationPage;
import com.safaricom.pages.services.RedeemBongaPointFinalConfirmationPage;
import com.safaricom.pages.services.RedeemBongaPointPage;
import com.safaricom.pages.services.RedemptionHistoryConfirmationPage;
import com.safaricom.pages.services.RedemptionHistoryPage;
import com.safaricom.pages.services.SambazaAirtimeConfirmationPage;
import com.safaricom.pages.services.SambazaAirtimePage;
import com.safaricom.pages.services.SambazaDataConfirmationPage;
import com.safaricom.pages.services.SambazaDataPage;
import com.safaricom.pages.services.SambazaservicesPage;
import com.safaricom.pages.services.FlexServiceStopAutoRenew_ConfirmationPage;
import com.safaricom.pages.services.TransferBongaPointConfirmationPage;
import com.safaricom.pages.services.TransferBongaPointFinalConfirmationPage;
import com.safaricom.pages.services.TransferBongaPointPage;
import com.safaricom.pages.services.TransferBongaPointServicePINPage;
import com.safaricom.pages.sfc.ContactUSHomePage;
import com.safaricom.pages.sfc.ServicesHomePage;
import com.safaricom.pages.sfc.sfcHomePage;
import java.io.IOException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.offset.PointOption;

@Test
public class ContactUsTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();

	public sfcHomePage sfcHomePageObject = new sfcHomePage(driver);
	public ServicesHomePage servicesHomePageObject = new ServicesHomePage(driver);
	public ContactUSHomePage contactUshomePageObject=new ContactUSHomePage(driver);
	public ContactUS_NewRequest contactUS_NewRequestObject=new ContactUS_NewRequest(driver);
	public ContactUS_NewRequestFinalConfirmation  contactUS_NewRequestFinalConfirmationObject=new ContactUS_NewRequestFinalConfirmation(driver);
	
	/* Verify whether there is option to create new Request. Able to connect with Youtube,Blog,Google etc. Title should be "CONTACT US" */
	
	@Test
	public void CONTACTUS_TC_001() throws InterruptedException, IOException {

		Thread.sleep(1000);

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.contactUs_click)).click();
		}
		String expected_maintitle = "CONTACT US";
		String actual_maintitle = contactUshomePageObject.txt_title.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		Assert.assertEquals(true, contactUshomePageObject.img_twitter_click.isDisplayed(), "Twitter is not displayed");
		Assert.assertEquals(true, contactUshomePageObject.img_facebook_click.isDisplayed(),"Facebook is not displayed");
		Assert.assertEquals(true, contactUshomePageObject.img_message_click.isDisplayed(), "Message is not displayed");
		Assert.assertEquals(true, contactUshomePageObject.new_request_click.isDisplayed(), "New Request button is not displayed");
		Assert.assertEquals(true, contactUshomePageObject.txt_youtube_click.isDisplayed(), "Youtube is not displayed");
		Assert.assertEquals(true, contactUshomePageObject.txt_blog_click.isDisplayed(), "Blog is not displayed");
		Assert.assertEquals(true, contactUshomePageObject.txt_google_plus_click.isDisplayed(), "Google Plus is not displayed");
		
		wait.until(ExpectedConditions.elementToBeClickable(contactUshomePageObject.new_request_click)).click();
		
		String expected_subtitle = "REQUEST";
		String actual_subtitle = contactUS_NewRequestObject.txt_title.getText();
		Assert.assertEquals(actual_subtitle, expected_subtitle);
		wait.until(ExpectedConditions.elementToBeClickable(contactUS_NewRequestObject.request_area_spinner)).click();
		wait.until(ExpectedConditions.elementToBeClickable(contactUS_NewRequestObject.request_area)).click();
		wait.until(ExpectedConditions.elementToBeClickable(contactUS_NewRequestObject.request_sub_area_spinner)).click();
		wait.until(ExpectedConditions.elementToBeClickable(contactUS_NewRequestObject.request_sub_area)).click();
		wait.until(ExpectedConditions.elementToBeClickable(contactUS_NewRequestObject.requst_msg)).sendKeys("New Request");
		wait.until(ExpectedConditions.elementToBeClickable(contactUS_NewRequestObject.request_submit)).click();
		wait.until(ExpectedConditions.elementToBeClickable(contactUS_NewRequestFinalConfirmationObject.tv_ok)).click();
		
		
	}

}
