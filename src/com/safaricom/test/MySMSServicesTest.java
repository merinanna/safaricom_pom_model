package com.safaricom.test;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.login.LocationPermissionPage;
import com.safaricom.pages.login.LoginFailurePage;
import com.safaricom.pages.login.LoginGeneratePinPage;
import com.safaricom.pages.login.LoginSuccessPage;
import com.safaricom.pages.login.ManagePhonePermissionPage;
import com.safaricom.pages.services.AccumulationHistoryConfirmationPage;
import com.safaricom.pages.services.BongaServicesPage;
import com.safaricom.pages.services.DSPlanActivateConfirmationPage;
import com.safaricom.pages.services.DSPlanActivateFinalConfirmationPage;
import com.safaricom.pages.services.DataandsmsplanservicesPage;
import com.safaricom.pages.services.DateSelectPage;
import com.safaricom.pages.services.FlexBuyBundlePage;
import com.safaricom.pages.services.FlexBuyBundle_ConfirmationPage;
import com.safaricom.pages.services.FlexBuyBundle_FinalConfirmationPage;
import com.safaricom.pages.services.FlexBuyBundle_PayWith;
import com.safaricom.pages.services.FlexBuyOtherConfirmationPage;
import com.safaricom.pages.services.FlexBuyOtherPage;
import com.safaricom.pages.services.FlexBuyother_FinalConfirmationPage;
import com.safaricom.pages.services.Flexservicespage;
import com.safaricom.pages.services.MysmsservicesDeactivateConfirmationPage;
import com.safaricom.pages.services.MysmsservicesDeactivateFinalConfirmationPage;
import com.safaricom.pages.services.MysmsservicesPage;
import com.safaricom.pages.services.RedeemBongaPointConfirmationPage;
import com.safaricom.pages.services.RedeemBongaPointFinalConfirmationPage;
import com.safaricom.pages.services.RedeemBongaPointPage;
import com.safaricom.pages.services.RedemptionHistoryConfirmationPage;
import com.safaricom.pages.services.RedemptionHistoryPage;
import com.safaricom.pages.services.FlexServiceStopAutoRenew_ConfirmationPage;
import com.safaricom.pages.services.TransferBongaPointConfirmationPage;
import com.safaricom.pages.services.TransferBongaPointFinalConfirmationPage;
import com.safaricom.pages.services.TransferBongaPointPage;
import com.safaricom.pages.services.TransferBongaPointServicePINPage;
import com.safaricom.pages.sfc.ServicesHomePage;
import com.safaricom.pages.sfc.sfcHomePage;
import java.io.IOException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.offset.PointOption;

@Test
public class MySMSServicesTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();

	public sfcHomePage sfcHomePageObject = new sfcHomePage(driver);
	public ServicesHomePage servicesHomePageObject = new ServicesHomePage(driver);
	public MysmsservicesPage mysmsservicesPageObject=new MysmsservicesPage(driver);
	public MysmsservicesDeactivateConfirmationPage mysmsservicesDeactivateConfirmationPageObject=new MysmsservicesDeactivateConfirmationPage(driver);
	public MysmsservicesDeactivateFinalConfirmationPage mysmsservicesDeactivateFinalConfirmationPageObject=new MysmsservicesDeactivateFinalConfirmationPage(driver);
	

	/*Verfiy whether Activated services are listed under "ACTIVATED"  tab and the title is "MY SMS SERVICES"*/
	
	@Test
	public void MYSMSSERVICES_TC_001() throws InterruptedException, IOException {

		Thread.sleep(1000);

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.services_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(servicesHomePageObject.mysmsservicesClick)).click();
		}
		String expected_maintitle = "MY SMS SERVICES";
		String actual_maintitle = mysmsservicesPageObject.mysmsservices_title.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		Assert.assertEquals(true, mysmsservicesPageObject.deactivateClick.isDisplayed(), "Deactivate is not displayed");
		
	}
	
	/*Verfiy whether user is able to deactivate activated service*/
	
	@Test
	public void MYSMSSERVICES_TC_002() throws InterruptedException, IOException {

		Thread.sleep(1000);

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.services_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(servicesHomePageObject.mysmsservicesClick)).click();
		}
		wait.until(ExpectedConditions.elementToBeClickable(mysmsservicesPageObject.deactivateClick)).click();
		wait.until(ExpectedConditions.elementToBeClickable(mysmsservicesDeactivateConfirmationPageObject.tv_ok)).click();
		wait.until(ExpectedConditions.elementToBeClickable(mysmsservicesDeactivateFinalConfirmationPageObject.tv_ok)).click();
	}

}
