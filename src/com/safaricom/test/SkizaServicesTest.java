package com.safaricom.test;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.login.LocationPermissionPage;
import com.safaricom.pages.login.LoginFailurePage;
import com.safaricom.pages.login.LoginGeneratePinPage;
import com.safaricom.pages.login.LoginSuccessPage;
import com.safaricom.pages.login.ManagePhonePermissionPage;
import com.safaricom.pages.services.AccumulationHistoryConfirmationPage;
import com.safaricom.pages.services.BongaServicesPage;
import com.safaricom.pages.services.DSPlanActivateConfirmationPage;
import com.safaricom.pages.services.DSPlanActivateFinalConfirmationPage;
import com.safaricom.pages.services.DataandsmsplanservicesPage;
import com.safaricom.pages.services.DateSelectPage;
import com.safaricom.pages.services.FlexBuyBundlePage;
import com.safaricom.pages.services.FlexBuyBundle_ConfirmationPage;
import com.safaricom.pages.services.FlexBuyBundle_FinalConfirmationPage;
import com.safaricom.pages.services.FlexBuyBundle_PayWith;
import com.safaricom.pages.services.FlexBuyOtherConfirmationPage;
import com.safaricom.pages.services.FlexBuyOtherPage;
import com.safaricom.pages.services.FlexBuyother_FinalConfirmationPage;
import com.safaricom.pages.services.Flexservicespage;
import com.safaricom.pages.services.RedeemBongaPointConfirmationPage;
import com.safaricom.pages.services.RedeemBongaPointFinalConfirmationPage;
import com.safaricom.pages.services.RedeemBongaPointPage;
import com.safaricom.pages.services.RedemptionHistoryConfirmationPage;
import com.safaricom.pages.services.RedemptionHistoryPage;
import com.safaricom.pages.services.SambazaAirtimeConfirmationPage;
import com.safaricom.pages.services.SambazaAirtimePage;
import com.safaricom.pages.services.SambazaDataConfirmationPage;
import com.safaricom.pages.services.SambazaDataPage;
import com.safaricom.pages.services.SambazaservicesPage;
import com.safaricom.pages.services.SkizaservicesConfirmationPage;
import com.safaricom.pages.services.SkizaservicesPage;
import com.safaricom.pages.services.FlexServiceStopAutoRenew_ConfirmationPage;
import com.safaricom.pages.services.TransferBongaPointConfirmationPage;
import com.safaricom.pages.services.TransferBongaPointFinalConfirmationPage;
import com.safaricom.pages.services.TransferBongaPointPage;
import com.safaricom.pages.services.TransferBongaPointServicePINPage;
import com.safaricom.pages.sfc.ServicesHomePage;
import com.safaricom.pages.sfc.sfcHomePage;
import java.io.IOException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.offset.PointOption;

@Test
public class SkizaServicesTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();

	public sfcHomePage sfcHomePageObject = new sfcHomePage(driver);
	public ServicesHomePage servicesHomePageObject = new ServicesHomePage(driver);
	public SkizaservicesPage skizaservicesPageObject=new SkizaservicesPage(driver);
	public SkizaservicesConfirmationPage skizaservicesConfirmationPage=new SkizaservicesConfirmationPage(driver);
	

	/* Verify whether there is option to search Skiza tunes with Artist and Song and the title should be "SKIZA SERVICES". */
	
	@Test
	public void SKIZASERVICES_TC_001() throws InterruptedException, IOException {

		Thread.sleep(1000);

		if (sfcHomePageObject.title.getText().equals("MENU")) {
			wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.services_click)).click();
			
		}
		
		wait.until(ExpectedConditions.elementToBeClickable(servicesHomePageObject.skizaservicesClick)).click();
		String expected_maintitle = "SKIZA SERVICES";
		String actual_maintitle = skizaservicesPageObject.skizaservices_title.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		
		Assert.assertEquals(true, skizaservicesPageObject.rbartist_click.isDisplayed(), "Artist Radio button is not displayed");
		Assert.assertEquals(true, skizaservicesPageObject.rbsong_click.isDisplayed(), "Song Radio button is not displayed");
		Assert.assertEquals(true, skizaservicesPageObject.artist_song.isDisplayed(), "Artist/Song field is not displayed");
		Assert.assertEquals(true, skizaservicesPageObject.search_btn_Click.isDisplayed(), "Search button is not displayed");
		
		wait.until(ExpectedConditions.elementToBeClickable(skizaservicesPageObject.rbartist_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(skizaservicesPageObject.artist_song)).sendKeys("A R RAHMAN");
		wait.until(ExpectedConditions.elementToBeClickable(skizaservicesPageObject.search_btn_Click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(skizaservicesConfirmationPage.tv_ok)).click();
		
	}

}
